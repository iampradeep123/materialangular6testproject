import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatSort, MatTableDataSource } from '@angular/material';

export interface PeriodicElement {
    name: string;
    position: number;
    designation: string;
    mobile: string;
    email: string;
    employer: string;
    location: string;
    managerName: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { position: 1, name: 'Suman', designation: 'Team Lead', mobile: '9939264347', email: 'mk@gmail.com', employer: 'BK Singh', location: 'HSR Layout', managerName: 'BK Singh' },
    { position: 2, name: 'Venkat', designation: 'QA', mobile: '9939987678', email: 'pk@gmail.com', employer: 'MK Singh', location: 'Bansankari', managerName: 'BK Singh' },
    { position: 3, name: 'Rajesh', designation: 'Dev', mobile: '9939987670', email: 'ik@gmail.com', employer: 'RK Singh', location: 'Bangaloe Cantt', managerName: 'BK Singh' },
    { position: 4, name: 'Amarnath', designation: 'Dev', mobile: '9039987678', email: 'vk@gmail.com', employer: 'SK Singh', location: 'Marathahalli', managerName: 'BK Singh' },
    { position: 5, name: 'Dlipp', designation: 'UX', mobile: '9000987678', email: 'gk@gmail.com', employer: 'ZK Singh', location: 'HSR Layout', managerName: 'BK Singh' }
];

@Component({
    selector: 'app-managerEmpDetails',
    templateUrl: './managerEmpDetails.component.html',
    styleUrls: ['./managerEmpDetails.component.css']
})
export class ManagerEmpDetailsComponent implements OnInit {
    //name, designation, mobile, email, employer, location,managerName
    displayedColumns: string[] = ['name', 'designation', 'mobile', 'email', 'employer', 'location', 'managerName'];
    dataSource = new MatTableDataSource(ELEMENT_DATA);

    @ViewChild(MatSort) sort: MatSort;
    modalRef: BsModalRef;

    selectedManagerObject: any;
    copySelectedManagerObject: any;

    constructor(
        private router: Router, private modalService: BsModalService,
    ) { }

    ngOnInit() {
        this.dataSource.sort = this.sort;
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    onShowManagerDetailsClick(element, template: TemplateRef<any>) {
        console.log(JSON.stringify(element));
        this.selectedManagerObject = element;
        this.copySelectedManagerObject = JSON.parse(JSON.stringify(element));
        this.modalRef = this.modalService.show(template);
    }

    onUpdateClick() {
        console.log(JSON.stringify(this.copySelectedManagerObject));
        this.selectedManagerObject.name = this.copySelectedManagerObject.name;
        this.selectedManagerObject.designation = this.copySelectedManagerObject.designation;
        this.selectedManagerObject.mobile = this.copySelectedManagerObject.mobile;
        this.selectedManagerObject.email = this.copySelectedManagerObject.email;
    }
}
